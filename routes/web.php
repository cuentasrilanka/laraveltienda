<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These  routes are loaded by the RouteServiceProvider within a group which  contains the "web" middleware group. Now create something great!
*/

Route::get('/', 'TestController@welcome');


Route::get('/prueba', function () {
    return 'Hola soy una ruta de prueba';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/logout', 'HomeController@logout')->name('logout');

Route::get('/search', 'SearchController@show');
Route::get('/products/json', 'SearchController@data');


//Parte del Publico, esta parte esta accesible por cualquier usuario que este logueado.
Route::get('/products/{id}', 'ProductController@show'); // muestra
Route::get('/categories/{miCategoria}', 'CategoryController@show'); // Aqui enviamos miCategoria al controlador CategoryController

Route::post('/cart', 'CartDetailController@store'); // Graba un producto en el carrito
Route::delete('/cart', 'CartDetailController@destroy'); // Elimina un articulo del carrito


Route::post('/order', 'CartController@update'); // graba el Pedido.


//Parte del Administrador, tanto las vistas como controladores esta organizado en la carpeta admin.
//Con el middleware nos aseguramos que el usuario esta autenticado y ademas tiene la marca de Admin = 1. Si no es así lo devuelve a la pag. de inicio.
Route::middleware(['auth', 'admin'])->prefix('admin')->namespace('Admin')->group(function () {
   
      //Aqui ya estamos suponiendo que estamos dentro de la carpeta admin:
      Route::get('/categories', 'CategoryController@index'); //Muesta los articulos de 10 en 10 gracias a la vista: view('admin.products.index');
      Route::get('/categories/create', 'CategoryController@create'); // Muestra un formulario para dar de alta Articulos gracias a la vista; view('admin.products.create');
      Route::get('/categories/{category}/edit', 'CategoryController@edit'); // Muestra un formulario para modificar Articulos ya existentes = view('admin.products.edit');
      Route::post('/categories', 'CategoryController@store'); // Este metodo del controlador es el que graba realmente el articulo en la BBDD cuando se lo solicita view('admin.products.create') mediante el formulario POST;
      Route::post('/categories/{category}/edit', 'CategoryController@update'); // Igual que el anterior, actualiza el articulo en la BBDD despues de que view('admin.products.edit') se lo solicite mediante el fomulario por metodo POST;
      Route::delete('/categories/{category}', 'CategoryController@destroy'); // Localiza el articulo mediante el $id y lo elimina de la BBDD

    //Poniendo el namespace se puede quitar el Admin  del ProductController = Route::get('/products', 'Admin\ProductController@index'); 

    //Aqui ya estamos suponiendo que estamos dentro de la carpeta admin:
    Route::get('/products', 'ProductController@index'); //Muesta los articulos de 10 en 10 gracias a la vista: view('admin.products.index');
    Route::get('/products/create', 'ProductController@create'); // Muestra un formulario para dar de alta Articulos gracias a la vista; view('admin.products.create');
    Route::get('/products/{id}/edit', 'ProductController@edit'); // Muestra un formulario para modificar Articulos ya existentes = view('admin.products.edit');
    Route::post('/products', 'ProductController@store'); // Este metodo del controlador es el que graba realmente el articulo en la BBDD cuando se lo solicita view('admin.products.create') mediante el formulario POST;
    Route::post('/products/{id}/edit', 'ProductController@update'); // Igual que el anterior, actualiza el articulo en la BBDD despues de que view('admin.products.edit') se lo solicite mediante el fomulario por metodo POST;
    Route::delete('/products/{id}', 'ProductController@destroy'); // Localiza el articulo mediante el $id y lo elimina de la BBDD
  
    // Route::get('/admin/products', 'Admin\ProductController@index'); //listado inicio
    // Route::get('/admin/products/create', 'Admin\ProductController@create'); // crear
    // Route::post('/admin/products', 'Admin\ProductController@store'); // crear
    // Route::get('/admin/products/{id}/edit', 'Admin\ProductController@edit'); // editar
    // Route::post('/admin/products/{id}/edit', 'Admin\ProductController@update'); // actualizar
    // Route::delete('/admin/products/{id}', 'Admin\ProductController@destroy'); // eliminar

    Route::get('/products/{id}/images', 'ImageController@index'); // Lista las imagenes asociadas al articulo que se pasa por $id
    Route::post('/products/{id}/images', 'ImageController@store'); //Sube una imagen a nuestro sitio web (carpeta /images/products) y la graba en la bbdd.

    // 
    Route::get('/products/{id}/images/select/{image}', 'ImageController@select'); // Busca un producto, y marca como destacada la imagen pasada como parametros.
    Route::delete('/products/{id}/images', 'ImageController@destroy'); // Elimina las imágenes.

});

//Es lo mismo que arriba pero abreviado.
Route::get('inicio', 'ProductController@index')->name('inicio'); //listado de inicio = Route::get('/admin/products', 'ProductController@index'); 
Route::get('productos', 'ProductController@index')->name('productos'); 

Route::post('editar/{id}', 'ProductController@update')->name('editar'); // actualizar
Route::post('borrar/{id}', 'ProductController@destroy')->name('borrar'); // Eliminar

