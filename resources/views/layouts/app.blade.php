<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  
  <title>
      @yield('title' , config('app.name'))  
   </title>
  
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/material-kit.css')}}" rel="stylesheet" />

  <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/material-kit.css" rel="stylesheet"/> -->


  @yield('styles')

  


</head>

<body class="@yield('body-class')">
 
<nav class="navbar navbar-transparent navbar-absolute">
      
    <div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<a class="navbar-brand" href="{{ url('/') }}"> {{ config('app.name') }} </a>
        	</div>

        <ul class="nav navbar-nav navbar-right">
          @guest

            <li> <a href="{{ route('login') }}"> Login </a></li>
            <li> <a href="{{ route('register') }}"> Registro </a></li>

          @else

          <li class="dropdown">	

          <a href="#" class ="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> 
          
            {{ Auth::user()->name }} 
            <span class="caret"></span>

          </a>

            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ url('/home') }}" role="button" aria-expanded="false"> Mi Sitio </a>
              </li>
              @if (auth()->user()->admin)
              <li>
                <a href="{{ url('/admin/categories') }}" role="button" aria-expanded="false"> Gestionar Categorias</a>
              </li>
              <li>
                <a href="{{ url('/admin/products') }}" role="button" aria-expanded="false"> Gestionar Productos</a>
              </li>
              @endif
          
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getDocumentById('logout-form').submit();">Cerrar Sesión </a>
              </li>
           </ul>
          </li>
          @endguest

        </ul>
        	<div class="collapse navbar-collapse" id="navigation-example">
          
          
        	</div>
    	</div>
</nav>

   <div class="wrapper" >

            @yield('contenido') 

    </div>
  </div>
  
    <!--   Core JS Files   -->
  <script src="{{ asset('/js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>

  <!-- <script src="{{ asset('/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script> -->
  <script src="{{ asset('js/material.min.js')}}"></script>

  <script src="{{ asset('/js/plugins/moment.min.js') }}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{ asset('/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{ asset('/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('/js/material-kit.js') }}" type="text/javascript"></script>
  
	@yield('scripts')
  
</body>

</html>



