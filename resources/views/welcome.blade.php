@extends('layouts.app')

@section('title', 'Bienvenido a ' . config('app.name'))

@section('body-class', 'landing-page')

@section('styles')

	<style>
		.team .row .col-md-4{
			margin-top:5em;
		}
		.team .row {
			display: flex;
			flex-wrap:wrap;
		}

		.team .row > [class*='col-']{
			display: flex;
			flex-direction: column;
		}
	</style>

@endsection

@section('contenido')

 <div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
            <div class="container">
                <div class="row">
					<div class="col-md-6">
						<h1 class="title">Bienvenido a {{config('app.name')}}</h1>
	                    <h4>Realiza pedidos en línea.</h4>
	                    <br />
	                    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" class="btn btn-danger btn-raised btn-lg">
							<i class="fa fa-play"></i> ¿Como funciona?
						</a>
					</div>
                </div>
            </div>
        </div>

		<div class="main main-raised">
			<div class="container">
		    	<div class="section text-center section-landing">
	                <div class="row">
	                    <div class="col-md-8 col-md-offset-2">
	                        <h2 class="title">Let's talk product</h2>
	                        <h5 class="description">This is the paragraph where you can write more details about your product. Keep you user engaged by providing meaningful information. Remember that by this time, the user is curious, otherwise he wouldn't scroll to get here. Add a button if you want the user to see more.</h5>
	                    </div>
	                </div>

					<div class="features">
						<div class="row">
		                    <div class="col-md-4">
								<div class="info">
									<div class="icon icon-primary">
										<i class="material-icons">chat</i>
									</div>
									<h4 class="info-title">First Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
		                    </div>
		                    <div class="col-md-4">
								<div class="info">
									<div class="icon icon-success">
										<i class="material-icons">verified_user</i>
									</div>
									<h4 class="info-title">Second Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
		                    </div>
		                    <div class="col-md-4">
								<div class="info">
									<div class="icon icon-danger">
										<i class="material-icons">fingerprint</i>
									</div>
									<h4 class="info-title">Third Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
		                    </div>
		                </div>
					</div>
	            </div>

				<div class="section text-center">
	                <h2 class="title">Categorias Disponibles:</h2>

					<form class="form-inline" method="get" action="{{ url('/search') }}">
						<input type="text" placeholder="¿Que buscas?" class="form-control" name="query" id="search" />
						<button type="submit" class="btn btn-primary btn-just-icon" type="submit">
							<i class="material-icons">search</i>
						</button>
					</form> 

					<div class="team">
						<div class="row">
						@foreach ($misCategorias as $mCategoria)
                            <div class="col-md-4">                            
							    <div class="team-player">
																
								<img src="{{ $mCategoria->featured_image_url }}" alt="Imagen" class="img-raised img-circle">

                                <h4> 
									<a href="{{ url('/categories/'.$mCategoria->id) }}"> {{ $mCategoria->name}} </a>
								</h4>

                                <p class="description"> {{$mCategoria->description}} </p>
                         	       </div>
			                </div>
			                @endforeach 
			                
						</div>
					
					</div>
	            </div>


	        	<div class="section landing-section">
	                <div class="row">
	                    <div class="col-md-8 col-md-offset-2">
	                        <h2 class="text-center title">Trabaja con nosotros</h2>
							<h4 class="text-center description">Divida detalles acerca de su producto o empresa de trabajo en partes. Escribe algunas líneas sobre cada uno y contáctanos sobre cualquier colaboración adicional. Le responderemos en un par de horas.</h4>
	                        <form class="contact-form">
	                            <div class="row">
	                                <div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label">Your Name</label>
											<input type="email" class="form-control">
										</div>
	                                </div>
	                                <div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label">Your Email</label>
											<input type="email" class="form-control">
										</div>
	                                </div>
	                            </div>

								<div class="form-group label-floating">
									<label class="control-label">Your Messge</label>
									<textarea class="form-control" rows="4"></textarea>
								</div>

	                            <div class="row">
	                                <div class="col-md-4 col-md-offset-4 text-center">
	                                    <button class="btn btn-primary btn-raised">
											Send Message
										</button>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>

	            </div>
	        </div>

		@include('includes.footer')

		</div>

	        
	 @endsection


	 @section('scripts')

	 	<script src="{{ asset('/js/typeahead.bundle.min.js') }} " type="text/javascript">  </script>
		
		 <script>
		 	$(function(){

				 var productos = new Bloodhound({
					datumTokenizer: Bloodhound.tokenizers.whitespace,
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					prefetch: '{{ url("/products/json") }}'
					//local:['hola', 'camiseta', 'pantalones', 'zapatillas', 'zapatos', 'gorras']
				 });

				 $('#search').typeahead({
					 hint: true,
					 highlight: true,
					 minLength: 1
				 }, {
					name: 'productos',
					source: productos
				 });
			 });
		 </script>


	 @endsection


