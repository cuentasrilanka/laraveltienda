@extends('layouts.app')

@section('title', 'Marcador CryptoZone ')

@section('body-class', 'product-page')

@section('contenido')

 <div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
</div>

	<div class="main main-raised">
			<div class="container">

                <div class="section">
					
                <h2 class="title text-center">Area Personal</h2>

					@if (session('notificacion'))
                        <div class="alert alert-success" role="alert">
                            {{ session('notificacion') }}
                        </div>
                    @endif
		    		
                    <ul class="nav nav-pills nav-pills-primary" role="tablist">
	                <li class="active">
	                	<a href="#dashboard" role="tab" data-toggle="tab">
			                <i class="material-icons">dashboard</i>
		                	Carrito Compra
		                </a>
	                </li>
	                
	                <li>
		                <a href="#tasks" role="tab" data-toggle="tab">
	            		<i class="material-icons">list</i>
	            		Pedidos Realizados
	                	</a>
	                </li>
                    </ul>

				<hr>
				<p> <center><b>Tu carrito de la compra contiene {{ auth()->user()->cart->details->count() }} productos. </b></center></p>
				<hr>
					<table class="table">
		                <thead>
		                    <tr>
		                        <th class="text-left">#</th>
		                        <th class="col-md-2">Nombre</th>
		                    	<th class="text-right">Precio</th>
								<th class="text-right">Cantidad</th>
								<th class="text-right">Subtotal</th>
		                        <th class="text-right">Acciones</th>
		                    </tr>
		                </thead>
		                <tbody>
						@foreach (auth()->user()->cart->details as $detail)
		                    <tr>
		                        <td class="text-left">
									<img src=" {{ $detail->product->featured_image_url }}" height= "50px" >
								</td>
		                       
							    <td class="text-left">  
									<a href="{{ url('/products/'. $detail->product->id) }}" target="_blank"> {{ $detail->product->name }} </a>
								</td>
								
								<td class="text-right"> {{ $detail->product->price }} </td>
								
								<td class="text-right"> {{ $detail->quantity }} </td>

								<td class="text-right"> {{ $detail->quantity * $detail->product->price }} </td>
								
		                        <td class="td-actions text-right">
                          
									<form method="POST" action="{{ url('/cart') }}">
																				                            
										 {{ csrf_field() }}
										  <!-- Es igual que poner <input type="hidden" name="_token" value=" {{ csrf_token() }}">  pero mas corto. -->
										 {{ method_field('DELETE') }}
										 <!-- Es igual que poner <input type="hidden" name="_method" value="DELETE">  pero mas corto. -->

										<input type="hidden" name="cart_detail_id" value="{{ $detail->id }}">

										<button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
											<i class="fa fa-times"></i>
		                            	</button>
									</form>
		                            
									
		                            
		                        </td>
		                    </tr>
						   @endforeach						   
		            	</tbody>
		        	</table>
					<form method="post" action =" {{ url('/order') }}">
					{{ csrf_field() }}
						<div class="text-center">
							<button class="btn btn-primary btn-round">
								<i class="material-icons">done</i> Realizar Pedido
							</button>
						</div>
					</form>

	            </div>
	        	
	        </div>

		</div>

@include('includes.footer')	   
        
@endsection



