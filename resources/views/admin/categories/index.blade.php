@extends('layouts.app')

@section('title', 'Listado de Productos')

@section('body-class', 'product-page')

@section('contenido')

 <div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
           
</div>

		<div class="main main-raised">
			<div class="container">
		    	<div class="section text-center">
	                <h2 class="title">Categorias:</h2>

					<div class="team">
						<div class="row">
						
						<a href=" {{ url('/admin/categories/create') }} " class="btn btn-primary btn-round"> Nueva Categoria </a>
						
						<hr/>
						
						<table class="table">
		                <thead>
		                    <tr>
		                        <th class="text-left">#</th>
		                        <th class="col-md-2">Nombre</th>
		                        <th class="col-md-4">Descripción</th>
		                        <th class="text-right">Acciones</th>
		                    </tr>
		                </thead>
		                <tbody>
							@foreach ($MisCategorias as $miCategoria)
		                    <tr>
		                        <td class="text-left">{{ $miCategoria->id }}</td>
		                        <td class="text-left">  {{ $miCategoria->name }}</td>
								<td class="text-left"> {{ $miCategoria->description }}</td>
								<td class="td-actions text-right">
		                           
									<form method="POST" action="{{ url('admin/categories/' .$miCategoria->id) }}">
									
										<a href="#" type="button" rel="tooltip" title="Ver Categoria" class="btn btn-info btn-simple btn-xs">
		                               		 <i class="fa fa-info"></i>
			                            </a>

			                            <a href="{{ url('/admin/categories/' .$miCategoria->id. '/edit') }}" type="button" rel="tooltip" title="Editar Categoria" class="btn btn-success btn-simple btn-xs">
		    	                            <i class="fa fa-edit"></i>
		        	                    </a>

 										 {{ csrf_field() }}
										  <!-- Es igual que poner <input type="hidden" name="_token" value=" {{ csrf_token() }}">  pero mas corto. -->
										 {{ method_field('DELETE') }}
										 <!-- Es igual que poner <input type="hidden" name="_method" value="DELETE">  pero mas corto. -->
										<button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
											<i class="fa fa-times"></i>
		                            	</button>

									</form>
		                          
		                        </td>
		                    </tr>
						   @endforeach						   
		                </tbody>
		            	</table>
						
						{{ $MisCategorias->links() }}
						
						</div>
					</div>

	            </div>
	        </div>
		</div>

	@include('includes.footer')
@endsection

