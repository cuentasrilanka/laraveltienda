@extends('layouts.app')

@section('title', 'Listado de Productos')

@section('body-class', 'product-page')

@section('contenido')

 <div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
           
</div>

		<div class="main main-raised">
			<div class="container">
		    	<div class="section text-center">
	                <h2 class="title">Productos Disponibles:</h2>

					<div class="team">
						<div class="row">
						
						<a href=" {{ url('/admin/products/create') }} " class="btn btn-primary btn-round"> Nuevo Producto </a>
						
						<table class="table">
		                <thead>
		                    <tr>
		                        <th class="text-left">#</th>
		                        <th class="col-md-2">Nombre</th>
		                        <th class="col-md-4">Descripción</th>
		                        <th>Categoria</th>
		                        <th class="text-right">Precio</th>
		                        <th class="text-right">Acciones</th>
		                    </tr>
		                </thead>
		                <tbody>
							@foreach ($MisProductos as $miProducto)
		                    <tr>
		                        <td class="text-left">{{ $miProducto->id }}</td>
		                        <td class="text-left">  {{ $miProducto->name }}</td>
								<td class="text-left"> {{ $miProducto->description }}</td>
								<td class="text-left"> {{ $miProducto->category ? $miProducto->category->name : 'General' }}</td>
		                        <td class="text-right">&euro; {{ $miProducto->price }}</td></td>
		                        <td class="td-actions text-right">
		                           
									<form method="POST" action="{{ url('admin/products/' .$miProducto->id) }}">
									
										<a href="{{ url('/products/'.$miProducto->id ) }}}" type="button" rel="tooltip" title="Ver Producto" class="btn btn-info btn-simple btn-xs" target="_blank">
		                                <i class="fa fa-info"></i>
			                            </a>
			                            <a href="{{ url('/admin/products/'.$miProducto->id. '/edit') }}" type="button" rel="tooltip" title="Editar Producto" class="btn btn-success btn-simple btn-xs">
		    	                            <i class="fa fa-edit"></i>
		        	                    </a>

 										<a href="{{ url('/admin/products/'.$miProducto->id. '/images') }}" type="button" rel="tooltip" title="Imágenes" class="btn btn-warning btn-simple btn-xs">
		    	                            <i class="fa fa-image"></i>
		        	                    </a>

										 {{ csrf_field() }}
										  <!-- Es igual que poner <input type="hidden" name="_token" value=" {{ csrf_token() }}">  pero mas corto. -->
										 {{ method_field('DELETE') }}
										 <!-- Es igual que poner <input type="hidden" name="_method" value="DELETE">  pero mas corto. -->
										<button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
											<i class="fa fa-times"></i>
		                            	</button>
									</form>
		                            <!-- <a href="{{ url('/admin/products/' .$miProducto->id. '/delete') }}" type="button" rel="tooltip" title="Borrar" class="btn btn-danger btn-simple btn-xs"> 
									<i class="fa fa-times"></i>
		                            </A>-->
		                        </td>
		                    </tr>
						   @endforeach						   
		                </tbody>
		            	</table>
						
						{{ $MisProductos->links() }}
						
						</div>
					</div>

	            </div>
	        </div>
		</div>

	@include('includes.footer')
@endsection

