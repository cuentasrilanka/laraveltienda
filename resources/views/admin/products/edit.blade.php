@extends('layouts.app')

@section('title', 'Bienvenido a Tienda')

@section('body-class', 'product-page')

@section('contenido')

 <div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
</div>

		<div class="main main-raised">
			<div class="container">
		    	
	        	<div class="section">
					
					<h2 class="title text-center">Editar Producto</h2>

					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li> {{ $error }} </li>
							@endforeach
						</ul>
					</div>
					@endif

					<form method="post" action=" {{ url('/admin/products/' .$miProducto->id. '/edit') }} "> 
					<!-- <form method="post" action=" {{ route('editar', ['id' => $miProducto->id]) }} "> -->
					

						{{ csrf_field() }}

						<div class="col-sm-4">
							<div class="form-group label-floating">
									<label class="control-label">Nombre del producto</label>
									<input type="text" class="form-control" name="name" value="{{ $miProducto->name }}">
							</div>
						  
						  
						 	<div class="form-group label-floating">
									<label class="control-label">Descripción corta</label>
									<input type="text" class="form-control" name="description" value="{{ $miProducto->description }}">
							</div>
						  
							<div class="form-group label-floating">
									<label class="control-label">Precio Producto</label>
									<input type="number" step="0.01" class="form-control" name="price" value="{{ $miProducto->price }}">
							</div>

							<div class="form-group label-floating">
									<select class="form-group" name="category_id">
										<option value="0"> General </option>
										@foreach ($categories as $category)
										<option value="{{ $category->id }}" @if ($category->id == old('category_id', $miProducto->category_id)) selected @endif > {{ $category->name }} </option>
										@endforeach
									</select>
							</div>

							<textarea class="form-control" placeholder="Descripción extensa" rows="5" name="long_description"> {{ $miProducto->long_description }} </textarea>

							<button class="btn btn-primary text-center">Guardar Cambios</button>
							<!-- <a href=" {{ url('/admin/products') }}" class="btn btn-default">Cancelar</a> -->
							<a href=" {{ route('inicio') }}" class="btn btn-default">Cancelar</a>

						</div>
					</form>

	            </div>


	        	
	        </div>

		</div>

	   @include('includes.footer')
        
@endsection

