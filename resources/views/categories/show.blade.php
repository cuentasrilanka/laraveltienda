@extends('layouts.app')

@section('title', 'Marcador CryptoZone ')

@section('body-class', 'profile-page')

@section('styles')

	<style>
		.team{
			padding-bottom: 50px;
		}
		
		.team .row .col-md-4{
			margin-top:5em;
		}
		.team .row {
			display: flex;
			flex-wrap:wrap;
		}

		.team .row > [class*='col-']{
			display: flex;
			flex-direction: column;
		}
	</style>

@endsection


@section('contenido')


		<div class="header header-filter" style="background-image: url('/img/examples/city.jpg');"></div>

		<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
	                <div class="row">
	                    <div class="profile">
	                        <div class="avatar">
								<img src="{{ $miCategoria->featured_image_url }}" alt="Circle Image" class="img-circle img-responsive img-raised">
	                        </div>
							
							<h2> {{ $miCategoria->name }} </h2>

								<!-- Si existe la variable notificacion, la pinta -->
								@if (session('notificacion'))
								<br/>
                        			<div class="alert alert-success" role="alert">
                            			{{ session('notificacion') }}
                        			</div>
                    			@endif
	                       
	                    </div>
	                </div>
	                <div class="description text-center">
                        <p> {{ $miCategoria->description }} </p>
	                </div>

					<div class="team">
						<div class="row">
						@foreach ($products as $mProducto)
                            <div class="col-md-4">                            
							    <div class="team-player">

								<img src="{{ $mProducto->images()->first() ? $mProducto->images()->first()->image : '' }}" alt="Imagen" class="img-raised img-circle">

                                <h4> 
								<a href="{{ url('/products/'.$mProducto->id) }}"> {{ $mProducto->name}} </a>
								
								<br> 
									 <small class="text-muted">	{{ $mProducto->category ? $mProducto->category->name : 'General' }} </small> 
								</h4>

                                <p class="description"> {{$mProducto->description}} </p>
                         	       </div>
			                </div>
			                @endforeach 
			                
						</div>
						<div> {{ $products->links() }} </div>
					</div>

					
	            </div>
	        </div>
		

    </div>


@include('includes.footer')	   
        
@endsection



