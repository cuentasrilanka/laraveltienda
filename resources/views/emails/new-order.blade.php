<html>
<head>  
<tittle> Nuevo Pedido </tittle>
</head>
<body>
    <p> Se ha realizado un nuevo pedido. </p>

    <ul>
        <li>
            <strong> Nombre: </strong>
            {{ $user->name }}
        </li>

        <li>
            <strong> E-mail: </strong>
            {{ $user->email }}
        </li>

        <li>
            <strong> Fecha Pedido: </strong>
            {{$cart->order_date   }}
        </li>
    </ul>

    <p>
        <a href="{{ url('/admin/order/' . $cart->id) }}"> Haz click aqui </a>
    </p>

</body>
</html>