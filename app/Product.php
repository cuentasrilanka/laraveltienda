<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    public static $messages = [
        'name.required' => 'Necesita rellenar el nombre.',
        'name.min' => 'Minimo 3 caracteres.',             
        'description.required' => 'Necesita rellenar la descripción.',
        'description.min' => 'Minimo 15 caracteres.',              
        'price.required' => 'El precio es obligatorio.', 
        'price.min' => 'El precio es obligatorio.',               
    ];
   
    public static $rules = [
        'name' =>  'required|min:3',
        'description' =>  'required|max:200',
        'price' =>  'required|numeric|min:0'
    ];
    
    // Para ver a que categoria pertenece un producto
    public function category()
    {
        // Un producto pertenece a 1 categoria
        return $this->belongsTo(Category::class);
    }

    // Para acceder a las imagenes
    public function images()
    {
        // Un producto tiene muchas fotos
        return $this->hasMany(ProductImage::class);
    }

    public function getFeaturedImageUrlAttribute(){
        
        $featuredImage = $this->images()->where('featured', true)->first();

        if (!$featuredImage)
            $featuredImage = $this->images()->first();            

        if ($featuredImage) {
            return $featuredImage->url;
        }

        return  public_path().'/images/products/default.jpg';
    }

    public function getCategoryNameAttribute()
    {
            if ($this->category)
                return $this->category->name;
            
            return 'General';
    }
}
