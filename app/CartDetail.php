<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    public function product()
    {
        // CartDetail N     1 Product   Un producto puede aparecer en varios carritos de compra     
        return $this->belongsTo(Product::class);
    }
}
