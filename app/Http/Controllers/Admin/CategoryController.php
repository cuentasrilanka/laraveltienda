<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
     // Muestra los primeros 10 articulos.
     public function index(){
        // $misProductos = Product::all();

        $misCategorias = Category::orderBy('name')->paginate(10);
         //    $miProducto = DB::select("select name from products where id = 1");
     //    dd($miProducto);      
         
        return view('admin.categories.index')->with('MisCategorias', $misCategorias);
         //return view('admin.products.index')->with(compact('products'));
         
     }
 
     public function create(){
         return view('admin.categories.create')    ; //formulario de registro.
     }
 
     public function store(Request $request){
         //registrar el nuevo producto en la bbdd
 
        $this->validate($request, Category::$rules, Category::$messages);    
        Category::create($request->all()); // Asignacion maxiva

        // Otra forma
        //  Category::create([
        //         'name' => $request->input('name'),
        //         'description' => $request->input('description');         
        //  ]);

        // Otra forma
        //  $mCategoria = new Category();
        //  $mCategoria->name = $request->input('name');
        //  $mCategoria->description = $request->input('description');         
        //  $mCategoria->save(); // INSERT EN CATEGORIA
 
         return redirect("/admin/categories");
     }
 
     public function edit(Category $category)
     {
        // return "Mostrar ID: $id";
         
         //$miCategoria = Category::find($id);
         return view('admin.categories.edit')->with(compact('category')); //form de edicion
     }
 
     public function update(Request $request, Category $category)
     {
        
          $this->validate($request, Category::$rules, Category::$messages);   
         //  $mCategoria = Category::find($id);
        //  $mCategoria->name = $request->input('name');
        //  $mCategoria->description = $request->input('description');
        //  $mCategoria->save(); // UPDATE EN PRODUCTOS

        $category->update($request->all());
         return redirect("/admin/categories");
     }
 
     public function destroy(Category $category)
     {
         //$miProducto = Product::find($id);               
         $category->delete(); // DELETE EN PRODUCTOS
 
         return back();
     }
}
