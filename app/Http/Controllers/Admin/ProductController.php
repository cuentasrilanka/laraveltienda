<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    // Muestra los primeros 10 articulos.
    public function index(){
       // $misProductos = Product::all();
       $misProductos = Product::paginate(10);
    //    $miProducto = DB::select("select name from products where id = 1");
    //    dd($miProducto);      
        
       return view('admin.products.index')->with('MisProductos', $misProductos);
        //return view('admin.products.index')->with(compact('products'));
        
    }

    public function create(){

        $categories = Category::orderBy('name')->get();
        return view('admin.products.create')->with(compact('categories')); //formulario de registro.
    }

    public function store(Request $request){
        //registrar el nuevo producto en la bbdd


        $this->validate($request, Product::$rules, Product::$messages);    

        $mProducto = new Product();
        $mProducto->name = $request->input('name');
        $mProducto->description = $request->input('description');
        $mProducto->price = $request->input('price');
        $mProducto->long_description = $request->input('long_description');
        $mProducto->category_id = $request->category_id;
        $mProducto->save(); // INSERT EN PRODUCTOS

        return redirect("/admin/products");
    }

    public function edit($id)
    {
       // return "Mostrar ID: $id";
        $categories = Category::orderBy('name')->get();

        $miProducto = Product::find($id);
        //return view('admin.products.edit')->with('miProducto', $miProducto); //form de edicion
        return view('admin.products.edit')->with(compact('miProducto', 'categories'));//form de edicion
    }

    public function update(Request $request, $id)
    {
        // $messages = [
        //     'name.required' => 'Necesita rellenar el nombre',
        //     'name.min' => 'Nombre: Minimo 3 caracteres',             
        //     'description.required' => 'Necesita rellenar la descripción',
        //     'description.min' => 'Descripción: Minimo 15 caracteres',              
        //     'price.required' => 'El precio es obligatorio', 
        //     'price.min' => 'El precio no puede ser negativo',               
        //     'price.numeric' => 'Ingrese un precio valido',
        //     'long_description.required' => 'Rellene la descripción larga'
        // ];
       
        // $rules = [
        //     'name' =>  'required|min:3',
        //     'description' =>  'required|max:200|min:15',
        //     'price' =>  'required|numeric|min:0',
        //     'long_description' =>  'required|max:200|min:15'
        // ];

        $this->validate($request, Product::$rules, Product::$messages);   

        $mProducto = Product::find($id);
        $mProducto->name = $request->input('name');
        $mProducto->description = $request->input('description');
        $mProducto->price = $request->input('price');
        $mProducto->long_description = $request->input('long_description');
        $mProducto->category_id = $request->category_id;
        $mProducto->save(); // UPDATE EN PRODUCTOS

        return redirect("/admin/products");
    }

    public function destroy($id)
    {
        $miProducto = Product::find($id);

        
        $miProducto->delete(); // DELETE EN PRODUCTOS

        return back();
    }
}
