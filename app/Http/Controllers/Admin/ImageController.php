<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImage;
use File; 

class ImageController extends Controller
{
    // Lista las imagenes asociadas al articulo id
    public function index($id)
    {
        $product = Product::find($id);
        $images = $product->images()->orderBy('featured', 'desc')->get();
        return view('admin.products.images.index')->with(compact('product', 'images'));
    }

    //Esta funcion sube una imagen a nuestro sitio (carpeta /images/products) y la graba en la bbdd.
    public function store(Request $request, $id)
    {
        //Primero grabamos la imagen en nuestro proyecto
        $file = $request->file('foto');
         
        $path = public_path() . '/images/products';
        $fileName = uniqid() . $file->getClientOriginalName();
        $moved = $file->move($path, $fileName);

        // Creamos un registro en la BBDD en la tabla de product_images 
        if ($moved) {
            $productImage = new ProductImage();
            $productImage->image = $fileName;
            $productImage->product_id = $id;
            $productImage->save();
        }
        
        return back();
    }

    public function destroy(Request $request, $id)
    {
        //Primero eliminamos el archivo
         $productImage = ProductImage::find($request->image_id);    
         if (substr($productImage->image, 0, 4) === "http"){
             $deleted = true;
         } else {
             // ES NECESARIO DAR PERMISOS 777 A LA CARPETA PUBLIC DESDE SSH
             $fullPath = public_path() . '/images/products/' . $productImage->image;
             $deleted = File::delete($fullPath);
         }

        
        // Ahora eliminamos el registro de la imagen en la Base de Datos
        if ($deleted){
            $productImage->delete();
        }

        return back();    
    }

    // Busca un producto, y marca como destacada la imagen pasada como parametros.
    public function select($id, $image){

        ProductImage::where('product_id', $id)->update([
            'featured' => false
        ]);

        $productImage = ProductImage::find($image);
        $productImage->featured = true;
        $productImage->save();

        return back();
    }
}   
