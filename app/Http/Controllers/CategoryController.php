<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    // En web.php le pasamos una categoria. Tiene que llamarse miCategoria
    public function show(Category $miCategoria){
        
        $products = $miCategoria->products()->paginate(10);
        
        return view('categories.show')->with(compact('miCategoria', 'products'));
    }

}
