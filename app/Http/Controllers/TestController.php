<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class TestController extends Controller
{
    public function welcome(){
       
        //$productos = Product::all();
        $misCategorias = Category::has('products')->get();
        
        return view('welcome')->with(compact('misCategorias'));
        //return view('welcome')->with('Productos', $products);
    }
}
