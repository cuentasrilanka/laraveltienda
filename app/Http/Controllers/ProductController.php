<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function show($id){

        $product = Product::find($id);
        $images = $product->images;

        $imagesLeft = collect();
        $imagesRight = collect();

        $i = 0;

        foreach ($images as $image) {
        
            if ($i%2==0)
                $imagesLeft->push($image);
            else
                $imagesRight->push($image);

            $i++;
        }    

        return view('products.show')->with(compact('product', 'imagesLeft', 'imagesRight'));

        //
    }

}
