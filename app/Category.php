<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public static $messages = [
        'name.required' => 'Necesita rellenar el nombre',
        'name.min' => 'Nombre: Minimo 3 caracteres',             
        'description.required' => 'Necesita rellenar la descripción',
        'description.min' => 'Descripción: Minimo 15 caracteres'              
       ];
   
       public static $rules = [
        'name' =>  'required|min:3',
        'description' =>  'required|max:200|min:15',
                     
    ];

    protected $fillable = ['name', 'description'];

    // Para consultar los productos que pertenecen a una categoria
    public function products()
    {
        return $this->hasMany(Product::class);
    }    

    public function getFeaturedImageUrlAttribute()
    {
        $featuredProduct = $this->products()->first();
        return $featuredProduct->featured_image_url;
    }

}
